package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"sync"
)

func httpPort() string {
	port := os.Getenv("HTTP_PORT")
	if port != "" {
		return port
	}
	return "5000"
}

func httpsPort() string {
	port := os.Getenv("HTTPS_PORT")
	if port != "" {
		return port
	}
	return "7000"
}

func echoReply(conn net.Conn) {
	_, err := conn.Write([]byte("🔊 Echo 1.0.1\n"))
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Waiting for TCP Socket on %s", conn.RemoteAddr())

	buffer := bufio.NewScanner(conn)
	for buffer.Scan() {
		line := buffer.Text()

		log.Printf("Received %q from TCP Socket on %s", line, conn.RemoteAddr())

		_, err := conn.Write(echoReplyBytes(line))
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Replied %q to TCP Socket on %s", line, conn.RemoteAddr())
	}

	err = buffer.Err()
	if err != nil && !strings.Contains(err.Error(), "connection reset by peer") {
		log.Fatal(err)
	}

	log.Printf("Closing TCP Socket on %s", conn.RemoteAddr())
}

func echoReplyBytes(msg string) []byte {
	return []byte(msg + "\n")
}

func echoReverse(conn net.Conn) {
	_, err := conn.Write([]byte("🔊 Echo 1.0.1\n"))
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Waiting for TCP Socket on %s", conn.RemoteAddr())

	buffer := bufio.NewScanner(conn)
	for buffer.Scan() {
		line := buffer.Text()

		log.Printf("Received %q from TCP Socket on %s", line, conn.RemoteAddr())

		_, err := conn.Write(echoReverseBytes(line))
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Replied %q to TCP Socket on %s", line, conn.RemoteAddr())
	}

	err = buffer.Err()
	if err != nil && !strings.Contains(err.Error(), "connection reset by peer") {
		log.Fatal(err)
	}

	log.Printf("Closing TCP Socket on %s", conn.RemoteAddr())
}

func echoReverseBytes(msg string) []byte {
	runes := []rune(msg)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return []byte(string(runes) + "\n")
}

func startHttpServer(wg *sync.WaitGroup) {
	defer wg.Done()

	address := fmt.Sprint(":", httpPort())
	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Listening on %s", address)

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Accepting new TCP Socket on %s", conn.RemoteAddr())
		go echoReply(conn)
	}
}

func startHttpsServer(wg *sync.WaitGroup) {
	defer wg.Done()

	address := fmt.Sprint(":", httpsPort())
	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Listening on %s", address)

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Accepting new TCP Socket on %s", conn.RemoteAddr())
		go echoReverse(conn)
	}
}

func main() {
	wg := &sync.WaitGroup{}
	wg.Add(2)

	go startHttpServer(wg)
	go startHttpsServer(wg)

	wg.Wait()
}
