FROM golang:1.16-buster

MAINTAINER thomas@leroux.io

COPY go.mod /var/lib/novln/
WORKDIR /var/lib/novln/
RUN go mod download
COPY . /var/lib/novln/
RUN GOBIN=`pwd` go install

CMD [ "./echo-server-tcp" ]
